package fr.isika.cdi.si.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.isika.cdi.si.App;

public class AppTestDevOps {
	//ctrl shift O ==> pour importer tous les imports need pour la classe
	// lancer le test ==> clic droit run as ==> junit
	
	@Test 
	// "Hello Linux" = 2eme parametre == ce que l'on veut, resultat
	//3eme param == methode que l'on veut tester ==> on compare la valeur retourn� pas la mthode avec le 2eme parametre
	public void testerAfficherMessage() {
		assertEquals("Erreur de retour message", "Hello Linux", App.afficherMessage("Hello Linux"));
	}

}
